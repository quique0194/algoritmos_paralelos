#include <iostream>
using namespace std;

int main(){
	int x = 10;
	#pragma omp parallel private(x)
	{
		cout << x << endl;
	}
	return 0;
}