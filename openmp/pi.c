#include <stdio.h>

int main(int argc, char* argv[]){
    int n = strtol(argv[1], NULL, 10);
    int thread_count = strtol(argv[2], NULL, 10);
    double sum = 0;
    int i = 0;
    # pragma omp parallel for num_threads(thread_count) reduction(+:sum)
    for(i=0; i<n; i++){
        double elem = 1.0/(2*i+1);
        sum += (i%2)? -elem : elem;
    }
    printf("PI: %f\n", 4*sum);
    return 0;
}