/* An introduction to parallel programming - peter pacheco - pag 267*/

#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

typedef long long ll;

double rand_between_minus_one_and_one(){
	double n = (double)rand()/RAND_MAX;
	int sign = (rand()%2)? -1:1;
	return n * sign;
}

int main(int argc, char* argv[]){

	ll number_of_tosses = strtol(argv[1], NULL, 10);

	ll number_in_circle = 0;
	srand(time(0));
	# pragma omp parallel for reduction(+:number_in_circle)
	for(ll toss = 0; toss < number_of_tosses; ++toss){
		double x = rand_between_minus_one_and_one();
		double y = rand_between_minus_one_and_one();
		double distance_squared = x*x + y*y;
		if(distance_squared <= 1) number_in_circle++;
	}
	
	double pi_estimate = 4*number_in_circle / (double)number_of_tosses;
	cout << "pi estimate: " << pi_estimate << endl;
	return 0;	
}
