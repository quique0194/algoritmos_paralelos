#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int thread_count;

void count_sort(int *a, int n){
	int *temp = (int*)malloc(n*sizeof(int));
	# pragma omp parallel for num_threads(thread_count)
	for(int i=0; i<n; ++i){
		int count = 0;
		for(int j=0; j<n; ++j){
			if(a[j]<a[i]) count++;
			else if(a[i]==a[j] && j<i) count++;
		}
		temp[count] = a[i];
	}
	memcpy(a, temp, n*sizeof(int));
	free(temp); 
}

void print_array(int *a, int n){
	for(int i=0; i<n; ++i)
		printf("%d ", a[i]);
	printf("\n");
}

int main(int argc, char* argv[]){

	int n = strtol(argv[1], NULL, 10);
	thread_count = strtol(argv[2], NULL, 10);

	int a[n];
	srand(time(0));
	for(int i=0; i<n; ++i)
		a[i] = rand()%n;
	//print_array(a, n);	
	count_sort(a, n);
	//print_array(a, n);
	
	return 0;
}
