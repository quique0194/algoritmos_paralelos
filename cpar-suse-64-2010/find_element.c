/* --- Arquivo : find_element.c --- */

/* --- inicializacao () --- */
#include <stdlib.h>
#include "type_find_element.h"  /* typedef's */
#include "vsg_find_element.h"   /* shared global */
#include "vsl_find_element.h"   /* shared local  */
#include "vst_find_element.h"
#include "bibpar.h"

extern int             mi_id;
extern int             n_proc_macro_;
extern struct comp_loc *macro;
extern comp_gl_usu     *sharedG;
extern void            *sharedL;
extern void            *sharedS;
extern _comp_task_     *sharedT;

#include <stdio.h>
void init_mat ();   /* --- spec () */
/* -- rs400() : definicao de task  */
void init_mat () 
{
int i, j;


   if (! _barrier())                    /* --- acao6()  */
      longjmp ((*macro).fim_bar, 1);
      
 {

   if (setjmp ((*macro).fim_bar) == 0)  /* --- forall () */
      end_barrier();

{
   int i_, n_;
   n_ = (999 - 0 + 1) / n_proc_macro_;
   if (((999 - 0 + 1) % n_proc_macro_) != 0)
      n_++;
   i_ = 0 + mi_id * n_;

   for (i = i_; ((i < (i_ + n_)) && (i <= 999)); i++) {
        for(j=0; j<1000; ++j)
            sharedG->mat[i][j] = i+j;
    }
}
   if (! _barrier())    /* --- forall () */
      longjmp ((*macro).fim_bar, 1);
  }


   if (setjmp ((*macro).fim_bar) == 0) /* --- acao7()  */
      end_barrier();
}

void find ();   /* --- spec () */
/* -- rs400() : definicao de task  */
void find () 
{
int i, j;


   if (! _barrier())                    /* --- acao6()  */
      longjmp ((*macro).fim_bar, 1);
   ((__find__ *)sharedL)->found = 0;
       
 {

   if (setjmp ((*macro).fim_bar) == 0)  /* --- forall () */
      end_barrier();

{
   int i_, n_;
   n_ = (999 - 0 + 1) / n_proc_macro_;
   if (((999 - 0 + 1) % n_proc_macro_) != 0)
      n_++;
   i_ = 0 + mi_id * n_;

   for (i = i_; ((i < (i_ + n_)) && (i <= 999)); i++) {
        for(j=0; j<1000; ++j)
            if(sharedG->mat[i][j] == sharedG->x){
                ((__find__ *)sharedL)->found++;
                printf("Found in pos %d %d\n", i, j);
            }
    }
}
   if (! _barrier())    /* --- forall () */
      longjmp ((*macro).fim_bar, 1);
  }

    if(!((__find__ *)sharedL)->found)
        printf("Not found\n");

   if (setjmp ((*macro).fim_bar) == 0) /* --- acao7()  */
      end_barrier();
}

int main()
{

int pidf,id_wait;
void dt_shareG();

   mc_var_inic ();

   def_task (0, "init_mat", sizeof (__init_mat__));
   def_task (1, "find", sizeof (__find__));
   id_wait=sem_wait_inic(2);


   alloc_proc(10);
      
   pidf = exec_task (0, 10);  /* --- rs30() - create --- */
   if (pidf == 0)   {
      init_mat();
      end_task();            /* --- rs31() - create --- */
   }

    
   wait_proc (0);    /* --- wait_proc() --- */


    scanf("%d", &sharedG->x);
    while(sharedG->x!=0){
          
   pidf = exec_task (1, 10);  /* --- rs30() - create --- */
   if (pidf == 0)   {
      find();
      end_task();            /* --- rs31() - create --- */
   }

        
   wait_proc (1);    /* --- wait_proc() --- */

        scanf("%d", &sharedG->x);
    }

       wait_all();      /* --- rs307()  */
   end_program();
   remove_semaforo(id_wait);
return 0;
}

