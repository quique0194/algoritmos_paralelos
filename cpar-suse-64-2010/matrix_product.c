/* --- Arquivo : matrix_product.c --- */

/* --- inicializacao () --- */
#include <stdlib.h>
#include "type_matrix_product.h"  /* typedef's */
#include "vsg_matrix_product.h"   /* shared global */
#include "vsl_matrix_product.h"   /* shared local  */
#include "vst_matrix_product.h"
#include "bibpar.h"

extern int             mi_id;
extern int             n_proc_macro_;
extern struct comp_loc *macro;
extern comp_gl_usu     *sharedG;
extern void            *sharedL;
extern void            *sharedS;
extern _comp_task_     *sharedT;

#include <stdio.h>
void initA ();   /* --- spec () */
void initB ();   /* --- spec () */
void initC ();   /* --- spec () */
void initD ();   /* --- spec () */
void multiplyAB ();   /* --- spec () */
void multiplyCD ();   /* --- spec () */
/* -- rs400() : definicao de task  */
void initA () 
{
int i, j;


   if (! _barrier())                    /* --- acao6()  */
      longjmp ((*macro).fim_bar, 1);
       
 {

   if (setjmp ((*macro).fim_bar) == 0)  /* --- forall () */
      end_barrier();

{
   int i_, n_;
   n_ = (99 - 0 + 1) / n_proc_macro_;
   if (((99 - 0 + 1) % n_proc_macro_) != 0)
      n_++;
   i_ = 0 + mi_id * n_;

   for (i = i_; ((i < (i_ + n_)) && (i <= 99)); i++) {
        for(j=0; j<100; ++j)
            sharedG->A[i][j] = i+j;
    }
}
   if (! _barrier())    /* --- forall () */
      longjmp ((*macro).fim_bar, 1);
  }


   if (setjmp ((*macro).fim_bar) == 0) /* --- acao7()  */
      end_barrier();
}

/* -- rs400() : definicao de task  */
void initB () 
{
int i, j;


   if (! _barrier())                    /* --- acao6()  */
      longjmp ((*macro).fim_bar, 1);
       
 {

   if (setjmp ((*macro).fim_bar) == 0)  /* --- forall () */
      end_barrier();

{
   int i_, n_;
   n_ = (99 - 0 + 1) / n_proc_macro_;
   if (((99 - 0 + 1) % n_proc_macro_) != 0)
      n_++;
   i_ = 0 + mi_id * n_;

   for (i = i_; ((i < (i_ + n_)) && (i <= 99)); i++) {
        for(j=0; j<100; ++j)
            sharedG->B[i][j] = i+2*j;
    }
}
   if (! _barrier())    /* --- forall () */
      longjmp ((*macro).fim_bar, 1);
  }


   if (setjmp ((*macro).fim_bar) == 0) /* --- acao7()  */
      end_barrier();
}

/* -- rs400() : definicao de task  */
void initC () 
{
int i, j;


   if (! _barrier())                    /* --- acao6()  */
      longjmp ((*macro).fim_bar, 1);
       
 {

   if (setjmp ((*macro).fim_bar) == 0)  /* --- forall () */
      end_barrier();

{
   int i_, n_;
   n_ = (99 - 0 + 1) / n_proc_macro_;
   if (((99 - 0 + 1) % n_proc_macro_) != 0)
      n_++;
   i_ = 0 + mi_id * n_;

   for (i = i_; ((i < (i_ + n_)) && (i <= 99)); i++) {
        for(j=0; j<100; ++j)
            sharedG->C[i][j] = 2*i+3*j;
    }
}
   if (! _barrier())    /* --- forall () */
      longjmp ((*macro).fim_bar, 1);
  }


   if (setjmp ((*macro).fim_bar) == 0) /* --- acao7()  */
      end_barrier();
}

/* -- rs400() : definicao de task  */
void initD () 
{
int i, j;


   if (! _barrier())                    /* --- acao6()  */
      longjmp ((*macro).fim_bar, 1);
       
 {

   if (setjmp ((*macro).fim_bar) == 0)  /* --- forall () */
      end_barrier();

{
   int i_, n_;
   n_ = (99 - 0 + 1) / n_proc_macro_;
   if (((99 - 0 + 1) % n_proc_macro_) != 0)
      n_++;
   i_ = 0 + mi_id * n_;

   for (i = i_; ((i < (i_ + n_)) && (i <= 99)); i++) {
        for(j=0; j<100; ++j)
            sharedG->D[i][j] = 2*i+j;
    }
}
   if (! _barrier())    /* --- forall () */
      longjmp ((*macro).fim_bar, 1);
  }


   if (setjmp ((*macro).fim_bar) == 0) /* --- acao7()  */
      end_barrier();
}

/* -- rs400() : definicao de task  */
void multiplyAB () 
{
int i, j, k;


   if (! _barrier())                    /* --- acao6()  */
      longjmp ((*macro).fim_bar, 1);
   for(i=0; i<100; ++i)
        for(j=0; j<100; ++j)
            for(k=0; k<100; ++k)
                sharedG->M1[i][j] +=  sharedG->A[i][k] * sharedG->B[k][j];  

   if (setjmp ((*macro).fim_bar) == 0) /* --- acao7()  */
      end_barrier();
}

/* -- rs400() : definicao de task  */
void multiplyCD () 
{
int i, j, k;


   if (! _barrier())                    /* --- acao6()  */
      longjmp ((*macro).fim_bar, 1);
   for(i=0; i<100; ++i)
        for(j=0; j<100; ++j)
            for(k=0; k<100; ++k)
                sharedG->M2[i][j] +=  sharedG->C[i][k] * sharedG->D[k][j];  

   if (setjmp ((*macro).fim_bar) == 0) /* --- acao7()  */
      end_barrier();
}

void sum_M1_M2()
{
int i, j;


   for(i=0; i<100; ++i)
        for(j=0; j<100; ++j)
            sharedG->S[i][j] = sharedG->M1[i][j] + sharedG->M2[i][j];
}

int main()
{

int pidf,id_wait;
void dt_shareG();

   mc_var_inic ();

   def_task (0, "initA", sizeof (__initA__));
   def_task (1, "initB", sizeof (__initB__));
   def_task (2, "initC", sizeof (__initC__));
   def_task (3, "initD", sizeof (__initD__));
   def_task (4, "multiplyAB", sizeof (__multiplyAB__));
   def_task (5, "multiplyCD", sizeof (__multiplyCD__));
   id_wait=sem_wait_inic(6);


   alloc_proc(8);
    
      
   pidf = exec_task (0, 2);  /* --- rs30() - create --- */
   if (pidf == 0)   {
      initA();
      end_task();            /* --- rs31() - create --- */
   }

      
   pidf = exec_task (1, 2);  /* --- rs30() - create --- */
   if (pidf == 0)   {
      initB();
      end_task();            /* --- rs31() - create --- */
   }

      
   pidf = exec_task (2, 2);  /* --- rs30() - create --- */
   if (pidf == 0)   {
      initC();
      end_task();            /* --- rs31() - create --- */
   }

      
   pidf = exec_task (3, 2);  /* --- rs30() - create --- */
   if (pidf == 0)   {
      initD();
      end_task();            /* --- rs31() - create --- */
   }

    wait_all();

      
   pidf = exec_task (4, 1);  /* --- rs30() - create --- */
   if (pidf == 0)   {
      multiplyAB();
      end_task();            /* --- rs31() - create --- */
   }

      
   pidf = exec_task (5, 1);  /* --- rs30() - create --- */
   if (pidf == 0)   {
      multiplyCD();
      end_task();            /* --- rs31() - create --- */
   }

    wait_all();

    sum_M1_M2();

    int i , j;
    for(i=0; i<10; ++i){
        for(j=0; j<10; ++j)
            printf("%d ", sharedG->S[i][j]);
        printf("\n");
    }

   wait_all();      /* --- rs307()  */
   end_program();
   remove_semaforo(id_wait);
   return 0;
}

