/* --- Arquivo : standard_deviation.c --- */

/* --- inicializacao () --- */
#include <stdlib.h>
#include "type_standard_deviation.h"  /* typedef's */
#include "vsg_standard_deviation.h"   /* shared global */
#include "vsl_standard_deviation.h"   /* shared local  */
#include "vst_standard_deviation.h"
#include "bibpar.h"

extern int             mi_id;
extern int             n_proc_macro_;
extern struct comp_loc *macro;
extern comp_gl_usu     *sharedG;
extern void            *sharedL;
extern void            *sharedS;
extern _comp_task_     *sharedT;

#include <stdio.h>
#include <math.h>
// standard deviation

void init ();   /* --- spec () */
/* -- rs400() : definicao de task  */
void init () 
{
int i, j;


   if (! _barrier())                    /* --- acao6()  */
      longjmp ((*macro).fim_bar, 1);
      
 {

   if (setjmp ((*macro).fim_bar) == 0)  /* --- forall () */
      end_barrier();

{
   int i_, n_;
   n_ = (999 - 0 + 1) / n_proc_macro_;
   if (((999 - 0 + 1) % n_proc_macro_) != 0)
      n_++;
   i_ = 0 + mi_id * n_;

   for (i = i_; ((i < (i_ + n_)) && (i <= 999)); i++) {
        for(j=0; j<1000; ++j)
            sharedG->mat[i][j] = i%10;//i+j;
    }
}
   if (! _barrier())    /* --- forall () */
      longjmp ((*macro).fim_bar, 1);
  }


   if (setjmp ((*macro).fim_bar) == 0) /* --- acao7()  */
      end_barrier();
}

void calculate_average ();   /* --- spec () */
/* -- rs400() : definicao de task  */
void calculate_average () 
{
int sum = 0;

int i, j;


   if (! _barrier())                    /* --- acao6()  */
      longjmp ((*macro).fim_bar, 1);
   for(i=0; i<1000; ++i){
        for(j=0; j<1000; ++j){
            sum += sharedG->mat[i][j];
        }
    }
    sharedG->average = (double)sum / 1000000;

   if (setjmp ((*macro).fim_bar) == 0) /* --- acao7()  */
      end_barrier();
}

void calculate_sd ();   /* --- spec () */
// standard deviation

/* -- rs400() : definicao de task  */
void calculate_sd () 
{
double sum = 0;

int i, j;


   if (! _barrier())                    /* --- acao6()  */
      longjmp ((*macro).fim_bar, 1);
   for(i=0; i<1000; ++i){
        for(j=0; j<1000; ++j){
            double t = sharedG->mat[i][j] - sharedG->average;
            t = t*t;
            sum += t;
        }
    }
    sharedG->sd = sqrt(sum/1000000);

   if (setjmp ((*macro).fim_bar) == 0) /* --- acao7()  */
      end_barrier();
}

int main()
{

int pidf,id_wait;
void dt_shareG();

   mc_var_inic ();

   def_task (0, "init", sizeof (__init__));
   def_task (1, "calculate_average", sizeof (__calculate_average__));
   def_task (2, "calculate_sd", sizeof (__calculate_sd__));
   id_wait=sem_wait_inic(3);


   alloc_proc(10);

      
   pidf = exec_task (0, 10);  /* --- rs30() - create --- */
   if (pidf == 0)   {
      init();
      end_task();            /* --- rs31() - create --- */
   }

    
   wait_proc (0);    /* --- wait_proc() --- */


      
   pidf = exec_task (1, 10);  /* --- rs30() - create --- */
   if (pidf == 0)   {
      calculate_average();
      end_task();            /* --- rs31() - create --- */
   }

    
   wait_proc (1);    /* --- wait_proc() --- */


      
   pidf = exec_task (2, 10);  /* --- rs30() - create --- */
   if (pidf == 0)   {
      calculate_sd();
      end_task();            /* --- rs31() - create --- */
   }

    
   wait_proc (2);    /* --- wait_proc() --- */


    printf("average:\t\t %f\n", sharedG->average);
    printf("standard deviation:\t %f\n", sharedG->sd);

       wait_all();      /* --- rs307()  */
   end_program();
   remove_semaforo(id_wait);
return 0;
}

