#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>

#define MY_BCAST_TAG 986194 // Random number

void print_vector(int* v, int size){
    for(int i=0; i<size; ++i)
        printf("%d ",v[i]);
    printf("\n");
}

void my_bcast(void* buf, int n, MPI_Datatype type, int origin, MPI_Comm group){
    int rank; MPI_Comm_rank(group, &rank);
    int procs; MPI_Comm_size(group, &procs);

    if(rank==origin){
        for(int i=0; i<procs; ++i)
            if(i!=origin)
                MPI_Send(buf, n, type, i, MY_BCAST_TAG, group);
    }
    else{
        MPI_Recv(buf, n, type, origin, MY_BCAST_TAG, group, MPI_STATUS_IGNORE);
    }
}

int main(int argc, char* argv[]){
    MPI_Init(&argc, &argv);
 
    // Get rank
    int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int procs; MPI_Comm_size(MPI_COMM_WORLD, &procs);

    // Reading size of buffer
    if(argc<2){
        if(rank==0) printf("You must specify the size of buffer(positive) and the broadcast function(0:mpi, 1:mine), e.g.\n./program 1000 1\n");
        MPI_Finalize();
        return 0;
    }
    int n = strtol(argv[1],NULL,10);
    int bcast_fn = strtol(argv[2],NULL,10);
    if(n<=0){
        if(rank==0) printf("The size of the buffer must be positive\n");
        MPI_Finalize();
        return 0;
    }

    // Init buffer
    int buf[n];
    if(rank==0){
        for(int i=0; i<n; ++i)
           buf[i] = i;
    }

    // Broadcast
    if(bcast_fn){
        my_bcast(buf, n, MPI_INT, 0, MPI_COMM_WORLD);
    }
    else{
        MPI_Bcast(buf, n, MPI_INT, 0, MPI_COMM_WORLD);
    }

    // Show results
    if(procs<=10 && n<=10)
        print_vector(buf,n);
    
    MPI_Finalize();
    return 0;
}
