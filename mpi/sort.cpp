#include <stdio.h>
#include <mpi.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>

#define N 1000        // Size of vector to sort


void init_rand_vector(int *v, int size, int max=1000){
	srand(time(0));
	for(int i=0; i<size; ++i)
		v[i] = rand()%max;
}

void print_vector(int* v, int size){
	for(int i=0; i<size; ++i)
		printf("%d ",v[i]);
	printf("\n");
}

// vector a of size m
// vector b of size n
// ret must be different from a and b
void merge(int* a, int m, int* b, int n, int* ret){
    int i=0, j=0;
    for(int k=0; k<m+n; ++k){
        if(i==m) ret[k] = b[j++];
        else if(j==n) ret[k] = a[i++];
        else if(a[i]<b[j]) ret[k] = a[i++];
        else ret[k] = b[j++];
    }
}

int main(int argc, char* argv[]){
	MPI_Init(&argc, &argv);

	// Get rank and number of proceses
	int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	int procs; MPI_Comm_size(MPI_COMM_WORLD, &procs);

    // Verify if number of procs is divisor of N
    if(N%procs){
        if(rank==0) printf("Number of proceses must be divisor of %d\n",N);
        return 0;
    }

	// Init vector
    int* v = NULL;
    if(rank==0){
        v = new int[N];
        init_rand_vector(v,N);
        print_vector(v,N);
    }

    // Scatter, sort and gather
    int part = N/procs;
    int temp[part];
    
    MPI_Scatter(v, part, MPI_INT, temp, part, MPI_INT, 0, MPI_COMM_WORLD);
    std::sort(temp,temp+part);
    MPI_Gather(temp, part, MPI_INT, v, part, MPI_INT, 0, MPI_COMM_WORLD);

    // Merge of sorted parts
    if(rank==0){
        int res[N], temp[N];
        memcpy(res,v,part*sizeof(int));
        for(int i=1; i<procs; ++i){
            merge(res,part*i,&v[part*i],part,temp);
            memcpy(res,temp,part*(i+1)*sizeof(int));
        }
        print_vector(res,N);
    }

	MPI_Finalize();
	return 0;
}
