#include <stdio.h>
#include <string.h>
#include <pthread.h>

struct mat{
    mat(){ memset(data, 0, sizeof data);}
    int data[100][100];
};

mat A, B, C, D;

struct mats{
    mat* a;
    mat* b;
    mat* r;
};

void * initialize_A(void * arg){
    for(int i=0; i<100; ++i)
        for(int j=0; j<100; ++j)
            A.data[i][j] = i + j;
}

void * initialize_B(void * arg){
    for(int i=0; i<100; ++i)
        for(int j=0; j<100; ++j)
            B.data[i][j] = i + 2*j;
}

void * initialize_C(void * arg){
    for(int i=0; i<100; ++i)
        for(int j=0; j<100; ++j)
            C.data[i][j] = 2*i + 3*j;
}

void * initialize_D(void * arg){
    for(int i=0; i<100; ++i)
        for(int j=0; j<100; ++j)
            D.data[i][j] = 2*i + j;
}

void * multiply(void* ms){
    mats* m = (mats*)ms;    
    for(int i=0; i<100; ++i)
        for(int j=0; j<100; ++j)
            for(int k=0; k<100; ++k)
                m->r->data[i][j] += m->a->data[i][k] * m->b->data[k][j];  
}

void* sum(void* ms){
    mats* m = (mats*)ms;
    for(int i=0; i<100; ++i)
        for(int j=0; j<100; ++j)
            m->r->data[i][j] = m->a->data[i][j] + m->b->data[i][j];
}

int main(){
    pthread_t initA, initB, initC, initD, mult1, mult2, sum_mats;
    pthread_create(&initA, NULL, initialize_A, NULL);
    pthread_create(&initB, NULL, initialize_B, NULL);
    pthread_create(&initC, NULL, initialize_C, NULL);
    pthread_create(&initD, NULL, initialize_D, NULL);
    pthread_join(initA, NULL);
    pthread_join(initB, NULL);
    pthread_join(initC, NULL);
    pthread_join(initD, NULL);

    mat E, F;
    mats ab; mats cd;
    ab.a = &A; ab.b = &B; ab.r = &E;
    cd.a = &C; cd.b = &D; cd.r = &F;
    pthread_create(&mult1, NULL, multiply, &ab);
    pthread_create(&mult2, NULL, multiply, &cd);
    pthread_join(mult1, NULL);
    pthread_join(mult2, NULL);

    mat G;
    mats ef;
    ef.a = &E; ef.b = &F; ef.r = &G;
    pthread_create(&sum_mats, NULL, sum, &ef);
    pthread_join(sum_mats, NULL);

    for(int i=0; i<10; ++i){
        for(int j=0; j<10; ++j)
            printf("%d ", G.data[i][j]);
        printf("\n");
    }

    return 0;
}
