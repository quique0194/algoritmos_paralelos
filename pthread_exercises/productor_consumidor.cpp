#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define DELAY 1
#define MAX_RAND 10
#define N 10

int products[N]; 
int full = 0;

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

void * producer(void * arg){
    while(1){
        sleep(DELAY);
        pthread_mutex_lock(&mutex1);
        if(full==N){ pthread_mutex_unlock(&mutex1); continue;}
        int t = rand()%MAX_RAND;
        products[full] = t;
        full++;
        printf("Produced %d\n", t);
        pthread_mutex_unlock(&mutex1);    
    }
}

void * consumer(void *arg){
    while(1){
        sleep(DELAY);
        pthread_mutex_lock(&mutex1);
        if(full == 0){ pthread_mutex_unlock(&mutex1); continue;}
        full--;
        printf("\t\tConsumed %d\n", products[full]);
        pthread_mutex_unlock(&mutex1);
    }
}

int main()
{
    srand(time(0));
    pthread_t thread1, thread2;
    
    pthread_create(&thread1, NULL, producer, NULL);
    pthread_create(&thread2, NULL, consumer, NULL);
    
    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    
    return 0;
}